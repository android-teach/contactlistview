package vn.edu.ictu.android.contactlistview;

public class VuContact {
    private String name;
    private String phone;

    public VuContact(String name, String phone) {
        this.name = name;
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public VuContact setName(String name) {
        this.name = name;
        return this;
    }

    public String getPhone() {
        return phone;
    }

    public VuContact setPhone(String phone) {
        this.phone = phone;
        return this;
    }
}
