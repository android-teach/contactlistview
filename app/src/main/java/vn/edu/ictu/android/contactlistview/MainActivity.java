package vn.edu.ictu.android.contactlistview;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private List<VuContact> contactList = new ArrayList<>();
    private ContactAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        contactList.add(new VuContact("Chu Vụ", "09854321"));
        contactList.add(new VuContact("Chu Hoà", "012345789"));
        contactList.add(new VuContact("Đỗ Tới", "098534578"));
        contactList.add(new VuContact("Văn Hiệp", "09812345"));

        adapter = new ContactAdapter(this, 0, contactList);

        ListView lvContact = findViewById(R.id.lvContact);
        lvContact.setAdapter(adapter);
        lvContact.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                VuContact contact = contactList.get(position);
                call(contact);
            }
        });
    }

    private void call(VuContact contact) {
        Intent i = new Intent(Intent.ACTION_CALL);
        i.setData(Uri.parse("tel:" + contact.getPhone()));
        startActivity(i);
    }
}
