package vn.edu.ictu.android.contactlistview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class ContactAdapter extends ArrayAdapter<VuContact> {
    private Context context;
    private List<VuContact> contactList;

    public ContactAdapter(Context context, int resource, List<VuContact> objects) {
        super(context, resource, objects);

        this.context = context;
        this.contactList = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(context).inflate(R.layout.contact_item, null);

        TextView tvName = convertView.findViewById(R.id.tvName);
        TextView tvPhone = convertView.findViewById(R.id.tvPhone);

        // get contact at position
        VuContact contact = contactList.get(position);

        tvName.setText(contact.getName());
        tvPhone.setText(contact.getPhone());

        return convertView;
    }
}
